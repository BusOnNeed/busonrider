[![Gitter](https://badges.gitter.im/BusOD/community.svg)](https://gitter.im/BusOD/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

# BusON: bus on need

## Prerequisite

## Problem Statement

### Pollution

Driving alone in a personal vehicle wastes resources and is harmful to the environment. Thus, sharing public transport is more economical and sustainable by reducing traffic congestion and CO2 emissions.

### Common Destination

Most commuters have common destination. Hence public transport.


### Communication Asymmetry

The bus driver lacks real time knowledge of each bus stops. Riders lack real time notification for the bus drivers for pending ride requests.

### Data Driven

To enable data-driven development on public transit, a reliable bus ride data tracking platform is necessary.

### Security

Bus drivers lack a reliable rating system. For riders as well.

### Minority accommodation

Disabled lacks an end-to-end solution for bus riding.

### Bus Driver Shortage

Scenario 1:
 Shortage of existing drivers but have a pool of "ex-drivers" that is no longer driving as wage is too little.
Scenario 2:
 Shortage of drivers in total, as full-time bus driving is less appealing.
 "On about nine days since early August, Busit has had to drop some bus runs because it didn't have enough people to put behind the wheel, Facebook posts show." --- New Zealand Herald (https://www.stuff.co.nz/waikato-times/news/115522948/sickness-driver-shortages-behind-hamilton-bus-system-problems)


### In-Efficient Trips

Bus drives with few riders and drives to stations without any passengers.

## Vision

Aim to provide cheap, efficient, and reliable public transportation for communities.

### Market Research

## Solution Proposal

An app provides information symmetry for bus riders and drivers and peer to peer bus driving.


### Competitive Analysis


### NetLogo Modeling

We use Netlogo (https://ccl.northwestern.edu/netlogo/) tool to model the bus transportation system of Auckland city. The data sources include

1. Bus routes and bus stops come from Auckland transport open gis data (http://data-atgis.opendata.arcgis.com/).
2. Auckland city tiles come from Auckland coucil open data (https://data-aucklandcouncil.opendata.arcgis.com/)
3. Number of victims data come from Police open data (policedata.nz). Note that merging bus stops and crime data is done mannually and may have inaccurracy problems.
4. The bus usages is modelled randomly and follows the normal distribution (0 to 5 persons randomly pops up bus stations every hour).

The simulation results require further validation. 

## Use Cases
